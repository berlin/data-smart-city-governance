import { Card, Container, Grid, Typography } from "@mui/material";
import NewChapterComponent from "../helperComponents/newChapterComponent/newChapterComponent";
import { newChapterContent } from "../helperComponents/newChapterComponent/newChapterContent";

const ScenarioComponent = () => {
  return (
    <Container
      maxWidth="md"
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <NewChapterComponent
        text={newChapterContent[2].text}
        title={newChapterContent[2].title}
        max={300}
      ></NewChapterComponent>
    </Container>
  );
};

export default ScenarioComponent;
