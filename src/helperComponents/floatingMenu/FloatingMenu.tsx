import { Box, Link, Typography } from "@mui/material";
import "./floatingMenu.css";

const FloatingMenu = () => {
  const handleMenuItemClick = (id: any) => {
    const element = document.getElementById(id);
    if (element) {
      const offset = element.getBoundingClientRect().top + window.scrollY - 100;
      window.scrollTo({ top: offset, behavior: "smooth" });
    }
  };

  return (
    <Box className="floating-menu">
      <Typography>
        Möchtest du wissen wie es funktioniert?{" "}
        <a
          style={{ color: "#0a58ca", textTransform: "none", cursor: "pointer" }}
          onClick={() => handleMenuItemClick("section4")}
        >
          Zu den Daten und Methoden.
        </a>
      </Typography>
    </Box>
  );
};

export default FloatingMenu;
