export const modeMap: any = {
  all: "Gesamt",
  car: "Auto",
  ev: "E-Auto",
  bus: "Öffentlicher Nahverkehr",
  bike: "Fahrrad",
};
