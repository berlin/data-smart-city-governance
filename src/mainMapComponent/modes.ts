export const verkehr = ["bike", "bus", "ev", "car"];
export const emission = ["co2", "nox", "pm"];
export const colors = ["green", "orange", "blue", "red", "grey"];
export const emissionColors = ["#800080", "#51414F", "#AA336A"];
