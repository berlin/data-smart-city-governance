# Inhaltsverzeichnis
1. [Einleitung](#einleitung)
2. [Über das Dashboard](#über-das-dashboard)
3. [Beteiligte](#beteiligte)
    - [Inhaltliche Koordination & Wissenschaftliche Forschung](#inhaltliche-koordination--wissenschaftliche-forschung)
    - [Technischer Partner](#technischer-partner)
4. [Förderhinweis](#förderhinweis)
5. [Installationshinweise](#installationshinweise)

# Einleitung
Dieses Dashboard zur Simulation der Auswirkungen verschiedener Verkehrsmaßnahmen und auf die Luftschadstoffbelastung in Berlin wurde im Rahmen des Projekts ["Data & Smart City Governance am Beispiel von Luftgütemanagement"](https://www.hiig.de/project/data-smart-city-governance-am-beispiel-von-luftguetemanagement/) entwickelt. Der Einsatz des Prototyps dient der Entwicklung eines übertragbaren Data Governance Frameworks am konkreten Use Case Luftgüte. Das Dashboard bildet keine Planwerke der Berliner Verwaltung ab.

Das Projekt wird vom [Alexander von Humboldt Institut für Internet und Gesellschaft (HIIG)](https://www.hiig.de/) zusammen mit dem [Kompetenzzentrum Wasser Berlin (KWB)](https://www.kompetenz-wasser.de/de) als eine der Pilotmaßnahmen der Strategie [Gemeinsam Digital: Berlin](https://gemeinsamdigital.berlin.de/de/) im Rahmen der [Modellprojekte Smart Cities](https://www.smart-city-dialog.de/modellprojekte-smart-cities) durchgeführt. Die [Arup Deutschland GmbH](https://www.arup.com/de-de/offices/germany) hat als Auftragnehmerin alle Rohdaten aus der agentenbasierten Modellierung der [Senozon Deutschland GmbH](https://senozon.com/) weiterverarbeitet, die Emissionsberechnungen durchgeführt und alle daraus resultierenden Karten, Graphiken und Daten erstellt und in der Bedienoberfläche zusammengeführt.

# Über das Dashboard
<p align="center" style="font-style: italic;">Bessere Luft durch Verkehrswende? Dashboard zur Visualisierung der Auswirkungen von Verkehrsmaßnahmen auf die Luft in Berlin</p>

Auf diesem Dashboard zeigen wir die simulierten Auswirkungen unterschiedlicher Verkehrsmaßnahmen und ihren Beitrag zur Verbesserung der Luft in Berlin. Gemeinsam mit der Berliner Senatsverwaltung für Mobilität, Verkehr, Klimaschutz und Umwelt (SenMVKU) haben wir dazu verschiedene Szenarien entwickelt.

Die simulierten Szenarien lassen sich in drei Kategorien gliedern: Im Gegenwarts-Szenario  werden zunächst die Mobilität und die dadurch entstehenden Emissionen an einem durchschnittlichen Werktag im Jahr 2023 dargestellt. Die zweite Simulation zeigt die Mobilität und verkehrsbedingten Emissionen im Berliner Stadtgebiet im Jahr 2030 unter der Prämisse, dass die auf diesen Zeithorizont ausgerichteten Pläne der Verwaltung auch tatsächlich umgesetzt werden. Die dritte Kategorie setzt das Erreichen dieser Ziele voraus, geht jedoch noch weiter: In den sog. Planspielen werden Maßnahmen mit großen Auswirkungen auf die Emissionen simuliert. Diese Maßnahmen betreffen verschiedene Akteursgruppen in der Stadt jedoch auf verschiedene Weise und unterschiedlich stark. Auch wenn die Berliner Verwaltung über diese Maßnahmen grundsätzlich nachdenkt, ist aktuell offen, ob, wann und in welcher Form diese Maßnahmen in Berlin tatsächlich eingeführt werden. Es liegen bislang keine konkreten Beschlüsse vor.

Die Kalibrierung und Validierung des Gegenwarts-Szenarios erfolgt auf Grundlage verschiedener Daten. Die Simulationen aller zukünftigen Szenarien im Jahr 2030 können nicht kalibriert werden. Sie basieren auf Annahmen zur Entwicklung der Stadt sowie der Verkehrsmittelwahl der Berliner*innen unter den veränderten Parametern. Die genutzten Daten und Methoden werden auf dem Dashboard erklärt.

# Beteiligte
## Inhaltliche Koordination & Wissenschaftliche Forschung
[Alexander von Humboldt Institut für Internet und Gesellschaft gGmbH](https://www.hiig.de/)

[Kompetenzzentrum Wasser Berlin GmbH](https://www.kompetenz-wasser.de/de)
## Technischer Partner
[ARUP Deutschland GmbH](https://www.arup.com/de-de/offices/germany)

# Förderhinweis
Das Projekt “Data & Smart City Governance am Beispiel von Luftgütemanagement” wird gefördert von dem Regierenden Bürgermeister von Berlin – Senatskanzlei – aus Mitteln des Bundesministeriums für Wohnen, Stadtentwicklung und Bauwesen sowie der Kreditanstalt für Wiederaufbau.

# Installationshinweise
Dieses Dashboard wurde mit der [Create React App](https://github.com/facebook/create-react-app) erstellt.

`npm start`

Das Dashboard kann im Verzeichnis mit `npm start` im Entwickler-Modus gestartet werden. Via http://localhost:3000 kann das GUI im Browser geöffnet werden. Die Seite wird bei Veränderungen am Code neu geladen.

`npm test`

Der test runner kann im Dashboard-Verzeichnis mit `npm test` gestartet werden. Weitere Informationen zum test runner finden Sie [hier](https://create-react-app.dev/docs/running-tests/).

`npm run build`

Mit `npm run build` wird ein optimierter Produktions-Build des Dashboards durchgeführt. Dabei wird eine optimierte Version des Codes erstellt, Ressourcen minimiert und die Anwendung für den Produktionseinsatz vorbereitet. Das Dashboard ist nun bereit für den Einsatz!

## Hinweis zur Nutzung der Karten
Für die Darstellung der interaktiven Karten muss ein sog. Mapbox-Token erstellt werden. Eine Anleitung dazu finden Sie [hier](https://docs.mapbox.com/help/getting-started/access-tokens/). 
Der Token muss anschließend in `src/mainMapComponent/util.ts` in Zeile 51 eingefügt werden.